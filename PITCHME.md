
@snap[midpoint span-90 text-25]
#### Publication d'un site statique en CI/CD sur AWS
@snapend

@snap[north-west span-15]
![](assets/img/Sogeti-logo-2018.png)
@snapend

@snap[north-east span-15]
![](assets/img/AJC.png)
@snapend

@snap[south text-06 text-gray span-100]

Projet fin de formation DevOps AWS  

Formateur référent: François Goffinet

@snapend

---
@snap[north]
### Sommaire
@snapend

@snap[south-west list-content-concise span-60]
@ol[](false)
- Présentation de l'équipe 
- Contexte
- Scénario et objectifs
- Méthodologie
- Infrastructure
- Développement
- Simulation
- Conclusion et Perspectives
@olend
<br><br>
@snapend

---?image=assets/img/trombinoscope.png&position=middle&size=50%
@snap[north]
### Présentation de l'équipe
@snapend

---
@snap[north]
### Contexte Dev - Ops
@snapend

@snap[west span-60 text-10]
- Tâches "Dev": fabrication du produit   
- Tâches "Ops": délivrer le produit fini et maintenir l'infrastructure nécessaire  
- Etroite collaboration entre les équipes  
- Partage des connaissances  
- Objectif commun: Satisfaction client
@snapend

@snap[east span-40]
![](assets/img/devops00.png)
@snapend

---
@snap[north]
### Contexte Dev - Ops
@snapend

@snap[west span-50 text-10]
- 1 seule équipe  
- Cérémonies agiles (suivi quotidien, documentations,...)  
- Déploiement continu et automatisation
@snapend

@snap[east span-50]
![](assets/img/devops_agile.png)
@snapend

---?image=assets/img/outils_devops.png&position=middle&size=70%
@snap[north]
### Contexte Dev - Ops
@snapend

---
@snap[north]
### Scénario et objectifs
@snapend

- Générer un livre numérique (ebook) en différents formats (HTML, PDF, MOBI, EPUB) en intégration continue et en livraison continue à partir d'un contenu écrit en Markdown sur une infrastructure dans le nuage.  

- Livraison continue du document sur un site Web aux normes actuelles (IPv6, HTTPS et CDN) offrant les différents formats de lecture.  

- Solution fournie sous forme de code.

---
@snap[north]
### Scénario et objectifs
@snapend

Résultat attendu: fournir une solution sous forme de code informatique (repo git) avec une documentation la plus complète.  

La méthode de travail est celle qui devrait le mieux correspondre aux principes DevOps.

---?image=assets/img/1intro.png&size=auto&color=#CCDAE7

---
@snap[north]
### Méthodologie
@snapend

@snap[bottom span-150]
![](assets/img/methode.png)
@snapend

---
@snap[north text-20]
#### Processus
@snapend

---?image=assets/img/2foot.png&size=auto 85%&color=#CCDAE7
---?image=assets/img/3listingtache.png&size=auto 85%&color=#CCDAE7

---
### Equipe Infrastructure 

---
@snap[north text-20]
#### Objectifs
@snapend

@snap[span-100 text-10]
@ul[spaced]
- Construction et déploiement sur un Bucket S3 
- Mise en place de l'infrastructure nécessaire
- Comment intégrer les deux?
@ulend
@snapend

---
@snap[north span-100 text-20]
#### Méthode de configuration
@snapend

- Manuelle/Console 

---?image=assets/img/infra1.png&position=middle&size=65%
@snap[north span-100 text-10]
##### Hébergement d'un site web statique
@snapend

---
@snap[north span-100 text-20]
#### AWS Cloudformation
@snapend

Langage commun pour:

- Décrire
- Provisionner
- Ressources d'infrastructure

Notre environnement Cloud 

---
@snap[north text-20]
#### AWS Cloudformation
@snapend

@snap[bottom span-150]
![](assets/img/infra2.png)
@snapend

---
@snap[north text-20]
#### Illustration
@snapend

@snap[bottom span-150]
![](assets/img/infra3.png)
@snapend

---
@title[Instance EC2]

@snap[north-east text-20]
#### Instance EC2
@snapend

@snap[west span-55 text-10]
@ul[spaced]
- Création d'un groupe de sécurité
- Ajout de droits de sécurité pour le trafic
- Suppression et création de clés
- Création d'une image Ubuntu
- Lancement de l'instance EC2
@ulend
@snapend

@snap[east span-45]
@img[shadow](assets/img/ec2Kevin1.png)
@snapend

---
@title[Instance EC2]

@snap[north-east text-20]
#### Instance EC2
@snapend

@snap[north-west span-55 text-10]
@ul[spaced]
- Programme applicatif VM 
- Image docker GitLab Runner
@ulend
@snapend

@snap[bottom span-100]
@img[shadow](assets/img/ec2Kevin2.png)
@snapend

---
@snap[north text-20]
#### Processus
@snapend

@snap[bottom span-150]
![](assets/img/raouiaProcessus.png)
@snapend

---
@snap[north text-20]
#### Le Markdown
@snapend

@snap[bottom span-150]
![](assets/img/markDown.png)
@snapend

---
@snap[north text-20]
#### Le Markdown
@snapend

@snap[bottom span-150]
![](assets/img/markDown2.png)
@snapend

---
@snap[north span-100 text-20]
#### Le livre de jeu Gitbook
@snapend

@snap[bottom span-150]
![](assets/img/artefacts.png)
@snapend

---?image=assets/img/4visiondev-ops.png&size=auto&color=#CCDAE7
---?image=assets/img/5gitlab.png&size=auto&color=#CCDAE7
---?image=assets/img/6.png&size=auto&color=#CCDAE7
---?image=assets/img/7.png&size=auto&color=#CCDAE7
---?image=assets/img/8.png&size=auto&color=#CCDAE7
---?image=assets/img/9.png&size=auto&color=#CCDAE7
---?image=assets/img/91.png&size=auto&color=#CCDAE7

---
@snap[midpoint span-90 text-25]
#### Simulation 
@snapend

---
@snap[north]
### Conclusion
@snapend
